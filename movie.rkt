#lang racket

(require srfi/1)
(require srfi/13)
(require srfi/48)

;; advanced command-line processor


(define responses

  ;; basic question

   '((1 "What type of films do you like?")

  ;; film selections

    (2 "So you like comedy films?")
    (3 "So you like very scary films?")
    (4 "So you like thrillers films?")
    (5 "So you like animated films?")
    (6 "So you like horror films?")
    (7 "So you like scfi films?")
    (8 "So you like romantic films?")
    (9 "So you like documentary films?")
    
    
    
    ;; Searching films
    
    (10 "Searching for comedy films........................
    Please type C to continue searching or type S to stop searching")
    (11 "Searching for very scary films....................
    Please type C to continue searching or type S to stop searching")
    (12 "Searching for thrillers films.....................
    Please type C to continue searching or type S to stop searching")
    (13 "Searching for animated films.....................
     Please type C to continue searching or type S to stop searching")
    (14 "Searching for horror films.....................
    Please type C to continue searching or type S to stop searching")
    (15 "Searching for scfi films.........................
    Please type C to continue searching or type S to stop searching")
    (16 "Searching for romantic films.....................
    Please type C to continue searching or type S to stop searching")
    (17 "Searching for documentry films...................
    Please type C to continue searching or type S to stop searching")


    ;; Recommended films

    ;;comedy
    (21 " please choose your film -
     Meet The Parents 
     Mean Girls 
     Pulp Fiction
     Hot Fuzz
     There's Something About Mary
     ")

    ;;very scary
    (22 " please choose your film -
     1920
     bait
     Bary
     The Bay
     Blood
     ")

    ;;thrillers
    (23 " please choose your film -
     Se7en 
     Psycho 
     Oldboy 
     Vertigo 
     Inception
     ")

    ;;animated
    (24 " please choose your film -
     Frozen
     Toy Story
     The Lion King 
     Tangled 
     Aladdin
     ")

    ;;horror
    (25 " please choose your film -
     Scary Movie
     Nosferatu
     The Evil Dead
     The Thing
     Dead
     ")

    ;;scfi
    (26 " please choose your film -
     Alien
     Inception
     Blade Runner
     Metropolis 
     District
     ")

    ;;romantic
    (27 " please choose your film -
     Titanic
     Love Actually
     The Notebook 
     Annie Hall
     Casablanca
     ")

    ;;documentry
    (28 " please choose your film -
     Man on Wire
     Grizzly Man
     Inside Job
     Crumb 
     Sicko
     ")
    
    
    ;; Rating
    (31 " nice decision please type R to rate this flim, type B to go back or type E to exit")
    (32 " Please rate this flim from (one to ten) ")
    (33 " Thank you for your time, press E to exit or B to start again")))

    
(define decisiontable
  '(
    
    (1 ((comedy) 2) ((very scary) 3) ((thrillers) 4) ((animated) 5) ((horror) 6) ((scfi) 7) ((romantic) 8) ((documentary) 9))
    
    ;; comedy
    (2 ((some) 10) ((a lot) 10) ((yes) 10) ((no) 1) ((not really) 1))
    (10 ((C) 21) ((S) 0))
    (21 ((Meet The Parents) 31) ((Mean Girls) 31) ((Pulp Fiction) 31) ((Hot Fuzz) 31) ((There's Something About Mary) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))

    ;; very scary
    (3 ((some) 11) ((a lot) 11) ((yes) 11) ((no) 1) ((not really) 1))
    (11 ((C) 22) ((S) 0))
    (22 ((1920) 31) ((bait) 31) ((Bary) 31) ((The Bay) 31) ((Blood) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))

    ;; thrillers
    (4 ((some) 12) ((a lot) 12) ((yes) 12) ((no) 1) ((not really) 1))
    (12 ((C) 23) ((S) 0))
    (23 ((Se7en) 31) ((Psycho) 31) ((Oldboy) 31) ((Vertigo ) 31) ((Inception) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))
   
    ;; animated
    (5 ((some) 13) ((a lot) 13) ((yes) 13) ((no) 1) ((not really) 1))
    (13 ((C) 24) ((S) 0))
    (24 ((Frozen) 31) ((Toy Story) 31) ((The Lion King) 31) ((Tangled) 31) ((Aladdin) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))

    ;;horror
    (6 ((some) 14) ((a lot) 14) ((yes) 14) ((no) 1) ((not really) 1))
    (14 ((C) 25) ((S) 0))
    (25 ((Scary Movie) 31) ((Nosferatu) 31) ((The Evil Dead) 31) ((The Thing) 31) ((Dead) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))

    ;;scfi
    (7 ((some) 15) ((a lot) 15) ((yes) 15) ((no) 1) ((not really) 1))
    (15 ((C) 26) ((S) 0))
    (26 ((Alien) 31) ((Inception) 31) ((Blade Runner) 31) ((Metropolis) 31) ((District) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))

    ;;romantic
    (8 ((some) 16) ((a lot) 16) ((yes) 16) ((no) 1) ((not really) 1))
    (16 ((C) 27) ((S) 0))
    (27 ((Titanic) 31) ((Love Actually) 31) ((The Notebook) 31) ((Annie Hall) 31) ((Casablanca) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))
    
    ;;documentary
    (9 ((some) 17) ((a lot) 17) ((yes) 17) ((no) 1) ((not really) 1))
    (17 ((C) 28) ((S) 0))
    (28 ((Man on Wire) 31) ((Grizzly Man) 31) ((Inside Job) 31) ((Crumb) 31) ((Sicko) 31))
    (31 ((R) 32) ((B) 1) ((E) 0))
    (32 ((one) 33) ((two) 33) ((three) 33) ((four) 33) ((five) ) ((six) 33) ((seven) 33) ((eight) 33) ((nine) 33) ((ten) 33))
    (33 ((E) 0) ((B) 1))

    
    
    ))






(define (assq-ref assqlist id)
  (cdr (assq id assqlist)))

(define (assv-ref assqlist id)
  (cdr (assv id assqlist)))

(define (get-response id)
  (car (assq-ref responses id)))

(define (get-keywords id)
  (let ((keys (assq-ref decisiontable id)))
    (map (lambda (key) (car key)) keys)))


;; outputs a list in the form: (0 0 0 2 0 0)
(define (list-of-lengths keylist tokens)
  (map 
   (lambda (x)
     (let ((set (lset-intersection eq? tokens x)))
       ;; apply some weighting to the result
       (* (/ (length set) (length x)) (length set))))
   keylist))

(define (index-of-largest-number list-of-numbers)
  (let ((n (car (sort list-of-numbers >))))
    (if (zero? n)
      #f
      (list-index (lambda (x) (eq? x n)) list-of-numbers))))


(define (lookup id tokens)
  (let* ((record (assv-ref decisiontable id))
         (keylist (get-keywords id))
         (index (index-of-largest-number (list-of-lengths keylist tokens))))
    (if index 
      (cadr (list-ref record index))
      #f)))


(define (recommend initial-id)
  (let loop ((id initial-id))
    (format #t "~a\n> " (get-response id))
    (let* ((input (read-line))
           (string-tokens (string-tokenize input))
           (tokens (map string->symbol string-tokens)))
      (let ((response (lookup id tokens)))
        (cond ((eq? #f response)
	       (format #t "huh? I didn't understand that! ")
	       (loop id)) 
	       ((zero? response)
	       (format #t "So Long, and Thanks for All the Fish...\n")
	       (exit))
	       (else
	       (loop response)))))))

(recommend 1)
